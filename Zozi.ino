#include <Adafruit_NeoPixel.h>

#define PIXEL_COUNT 150 // How many leds per led strip

// Function declaration
void colorWipe(Adafruit_NeoPixel *ledStrip, uint32_t color, int wait);

// Pinout mapping
const uint8_t ledStripPin[] = { 8, 7 };
const uint8_t pwmPin[] = { 4, 5, 6, 9, 10, 11 };

// Init NeoPixel objects
Adafruit_NeoPixel *ledStrip[sizeof(ledStripPin)];

void setup() {
  // Set led strips pinout
  for (int i = 0; i < sizeof(ledStripPin); i++) {
    ledStrip[i] = new Adafruit_NeoPixel(PIXEL_COUNT, ledStripPin[i], NEO_GRB + NEO_KHZ800);
    ledStrip[i]->begin();
    ledStrip[i]->show();
  }
  
  // Set pwm pins to output mode
  // and light on all bulbs (max. intensity)
  for (int i = 0; i < sizeof(pwmPin); i++) {
    pinMode(pwmPin[i], OUTPUT);
    digitalWrite(pwmPin[i], HIGH);
  }

  // End of setup
  // light on the built in led
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);  
}

void loop() {
  // Fill all led strips with full intensity RGB
  for (int i = 0; i < sizeof(ledStripPin); i++)
    colorWipe(ledStrip[i], ledStrip[i]->Color(255, 255, 255), 0);

  delay(1000); // Wait 1s
}

// Fill a ledStrip with a single color
void colorWipe(Adafruit_NeoPixel *ledStrip, uint32_t color, int wait) {
  for(int i=0; i<ledStrip->numPixels(); i++) {  //  For each pixel in strip...
    ledStrip->setPixelColor(i, color);          //  Set pixel's color (in RAM)
    ledStrip->show();                           //  Update strip to match
    delay(wait);                                //  Pause for a moment
  }
}
